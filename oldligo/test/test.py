from tools import ContractExecutor

INITIAL_STORAGE = "{ events=([]: event list); total_users=0n; current_phase=Insuring; insured_balance=0tez; remaining_balance=0tez; users=(Big_map.empty: (address, user_data) big_map); cells=(Big_map.empty: (nat, address set) big_map) }"

ce = ContractExecutor(INITIAL_STORAGE)
ce.initialize('tz1THsLcunLo8CmDm9f2y1xHuXttXZCpyFnq')
ce.insureUser({})
ce.insureUser({})