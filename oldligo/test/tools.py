import os
import subprocess

def dry_run(action, storage):
    out = subprocess.Popen(['ligo', 'dry-run', '../src/crowdsurance.mligo', '-s', 'cameligo', 'main', action, storage], 
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT)
    stdout, stderr = out.communicate()
    return stdout.decode("utf-8").replace(')\n\n', '').split(' ] , ')[1]


class ContractExecutor:
    storage = ''

    def __init__ (self, initial_storage):
        self.storage = initial_storage

    def initialize(self, pkey_hash):
        act = 'Initialize(\"' + pkey_hash + '\": key_hash)'
        st = dry_run(act, self.storage)
        print('Storage ->', st)
        self.storage = st

    def insureUser(self, udata):
        pass

