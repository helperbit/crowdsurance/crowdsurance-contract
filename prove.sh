LIB=~/.opam/default/share/archetype/mlw/

process() {
	why3 -L $LIB prove $1 > $1.proof 2>/dev/null
	RETP=$(echo $?)
	why3 -L $LIB prove -P alt-ergo $1 > $1.run.proof 2>/dev/null
	RETAE=$(echo $?)
	# why3 -L $LIB extract -D ocaml64  $1 > /dev/null 2>/dev/null
	# RETML=$(echo $?)

	printf '%-60s' $1
		
	if [ ${RETP} -eq 0 ]; then
		echo -ne "\033[32m OK \033[0m"
	else
		echo -ne "\033[31m KO \033[0m"
	fi
		
	if [ ${RETAE} -eq 0 ]; then
		echo -ne "\033[32m OK \033[0m"
	else
		echo -ne "\033[31m KO \033[0m"
	fi
		
	# if [ ${RETML} -eq 0 ]; then
	# 	echo -ne "\033[32m OK \033[0m"
	# else
	# 	echo -ne "\033[31m KO \033[0m"
	# fi

	echo ""
}

for file in out/*.mlw
do
	process "$file"
done